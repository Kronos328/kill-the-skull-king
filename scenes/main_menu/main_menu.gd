extends Node3D

var version = ProjectSettings.get_setting("application/config/version")

@onready
var version_info = $CanvasLayer/Control/VersionInfo

func _ready():
	version_info.text = "v %s" % version

func _on_quit_button_pressed():
	get_tree().quit()


func _on_start_pressed():
	get_tree().change_scene_to_file("res://main.tscn")
