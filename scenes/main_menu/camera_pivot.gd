extends Node3D

@export
var rotation_speed: float


func _process(delta):
	rotate_y(rotation_speed * delta)
