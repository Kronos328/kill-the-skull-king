extends Control

@export
var player: Node3D

@export
var game: Node3D

@onready
var health_bar = $HealthBar

@onready
var kills_bar = $KillsBar

func _ready():
	health_bar.max_value = player.starting_health
	health_bar.value = player.starting_health
	kills_bar.max_value = game.kills_to_next_level
	kills_bar.value = 0


func _on_player_health_changed(current_health):
	health_bar.value = current_health


func _on_player_setup_health(starting_health):
	health_bar.max_value = starting_health


func _on_game_kill_scored(current_kills):
	kills_bar.value = current_kills


func _on_game_leveled_up(current_level):
	kills_bar.value = 0
