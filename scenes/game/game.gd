extends Node3D

signal kill_scored(current_kills)
signal leveled_up(current_level)

@export
var upgrade_menu: PopupPanel

@onready
var player = $Player

var kills = 0

var level = 1

var kills_to_next_level = 6

func _input(event):
	if event.is_action_pressed("pause"):
		upgrade_menu.popup_centered()
		get_tree().paused = true


func _on_stray_projectile_killer_area_exited(area):
	area.get_parent().queue_free()


func _on_player_died():
	get_tree().change_scene_to_file("res://scenes/dead/dead.tscn")


func _on_enemy_died():
	print("Kill")
	kills += 1
	kill_scored.emit(kills)
	Scorekeeper.total_killed += 1
	
	if kills >= kills_to_next_level:
		level += 1
		leveled_up.emit(level)
		print("level up - %s" % level)
		kills = 0
