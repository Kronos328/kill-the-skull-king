extends Control

@onready
var dead_label = $DeadLabel


# Called when the node enters the scene tree for the first time.
func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	dead_label.text = "%s heroes were killed" % Scorekeeper.total_killed


func _on_button_pressed():
	get_tree().quit()
