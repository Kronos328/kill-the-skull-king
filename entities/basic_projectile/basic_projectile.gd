extends Node3D

@export
var speed: int

@export
var free_on_contact: bool = true

var fire_direction: Vector3

var damage: int

func _ready():
	look_at(fire_direction)

func _physics_process(delta):
	position += fire_direction * speed * delta


func _on_area_3d_body_entered(body):
	body.damage(damage)
	if free_on_contact:
		queue_free()


func fire(direction: Vector3, config_damage: int):
	fire_direction = direction
	damage = config_damage
