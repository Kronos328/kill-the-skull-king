extends Node3D

signal setup_health(starting_health: int)

signal health_changed(current_health: int)

signal died

#@export/
#var rotation_speed: float

@export
var mouse_sensitivity = 0.0005

@export
var starting_health = 50

@onready
var damage_audio = $DamageAudio

var mouse_velocity: Vector2

var current_health


func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	current_health = starting_health


func _process(delta):
	#_process_movement(delta)
	_process_movement_mouse(delta)


#func _process_movement(delta: float):
#	if Input.is_action_pressed("rotate_left"):
#		rotate_y(rotation_speed * delta)
#	elif Input.is_action_pressed("rotate_right"):
#		rotate_y(-rotation_speed * delta)


func _process_movement_mouse(delta: float):
	rotate_y(-Input.get_last_mouse_velocity().x * mouse_sensitivity * delta)


func _on_damage_zone_enemy_entered(body):
	current_health -= body.player_damage
	damage_audio.play()
	if current_health <= 0:
		died.emit()
	health_changed.emit(current_health)
	body.queue_free()
