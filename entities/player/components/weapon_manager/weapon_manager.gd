extends Node3D

@export
var projectile: PackedScene

@export
var fire_action_name: StringName

@export
var cooldown_seconds: float

@export
var active = false

@export
var weapon_damage: int

@export
var player: Node3D

@onready
var _cooldown_timer: Timer = $CooldownTimer

@onready
var _shot_audio = $ShotAudio


func _ready():
	_cooldown_timer.wait_time = cooldown_seconds


func activate():
	active = true


func _input(event):
	if not event.is_action_pressed(fire_action_name, true):
		return
	
	if not active:
		return
	
	if not _cooldown_timer.is_stopped():
		return
	
	var health_damage_bonus = 0
#
#	if abs(player.starting_health - player.current_health) < int(player.starting_health * 0.25):
#		health_damage_bonus = int(abs(player.starting_health - player.current_health))
#		print("Damage bonus = %s" % health_damage_bonus)
	
	var projectile_instance = projectile.instantiate()
	var fire_direction = player.global_position.direction_to(global_position)
	fire_direction.y = 0
	projectile_instance.fire(fire_direction, weapon_damage + health_damage_bonus)
	projectile_instance.position = global_position
	player.add_sibling(projectile_instance)
	_cooldown_timer.start()
	_shot_audio.play()
	

