extends CharacterBody3D

signal killed

@export
var move_speed: float

@export
var mesh: Node3D

@export
var starting_health: int

@export
var spawn_upgrade_threshold := 10

@export
var player_damage := 1

@export
var damage_audio: AudioStream

@onready
var life_bar = $SubViewport/Control/ProgressBar

var move_direction: Vector3

var current_health: int

func _ready():
	change_speed(move_speed)
	life_bar.max_value = starting_health
	life_bar.value = starting_health
	current_health = starting_health
	mesh.look_at(move_direction, Vector3(0,1,0), true)


func _physics_process(_delta):
	move_and_slide()


func pushback():
	var pushback_speed = 30
	change_speed(-pushback_speed)
	get_tree().create_timer(0.5).timeout.connect(
	func(): change_speed(move_speed)
	)


func change_speed(new_speed):
	velocity = Vector3(
		move_direction.x,
		0,
		move_direction.z
	)
	velocity *= new_speed


func damage(value):
	current_health -= value
	life_bar.value = current_health
	if current_health <= 0:
		var death_sound = AudioStreamPlayer3D.new()
		death_sound.stream = damage_audio
		death_sound.position = global_position
		add_sibling(death_sound)
		death_sound.finished.connect(death_sound.queue_free)
		death_sound.play()
		killed.emit()
		queue_free()


func scale_stats(spawned_amount: int):
	var upgrade_level = int(spawned_amount / spawn_upgrade_threshold)
	
	starting_health += upgrade_level
	
	player_damage += int(upgrade_level / 10)
	
	
	
