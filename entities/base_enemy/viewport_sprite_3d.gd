extends Sprite3D

@export
var viewport: SubViewport

func _ready():
	texture = viewport.get_texture()
