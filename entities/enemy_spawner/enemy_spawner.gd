extends Node3D

@export
var enemy_list: Array[PackedScene]

@export
var spawn_angle_step: int = 2

@onready
var spawn_marker = $SpawnMarker

var _spawn_rotations := []

@onready
var game = get_parent()

var spawned_amount = 0

func _ready():
	_generate_spawn_rotations()


func _generate_spawn_rotations():
	_spawn_rotations = range(0, 360, spawn_angle_step)
	_spawn_rotations.shuffle()


func _on_spawn_timer_timeout():
	randomize()
	var enemy_to_spawn = enemy_list.pick_random()
	var spawn_rotation = _spawn_rotations.pop_front() 
	rotation_degrees.y = spawn_rotation
	
	var enemy_instance = enemy_to_spawn.instantiate()
	enemy_instance.position = spawn_marker.global_position
	
	var direction_to_center = spawn_marker.global_position.direction_to(global_position)
	enemy_instance.move_direction = direction_to_center
	
	enemy_instance.scale_stats(spawned_amount)
	
	add_sibling(enemy_instance)
	
	enemy_instance.killed.connect(
		game._on_enemy_died,
		CONNECT_ONE_SHOT
	)
	
	spawned_amount += 1
	
	if _spawn_rotations.is_empty():
		_generate_spawn_rotations()
