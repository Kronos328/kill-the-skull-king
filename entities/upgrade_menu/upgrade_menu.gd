extends PopupPanel

@export
var game: Node3D

@export
var player: Node3D

@export
var basic_attack_upgrade_button: Button

@export
var mortar_upgrade_button: Button

@export
var shield_upgrade_button: Button


func _on_popup_hide():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	get_tree().paused = false


func _on_about_to_popup():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
