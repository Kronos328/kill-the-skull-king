extends TextureRect

@export
var viewport: SubViewport

# Called when the node enters the scene tree for the first time.
func _ready():
	texture = viewport.get_texture()


