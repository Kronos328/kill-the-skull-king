# Kill the Skull King?

A game made for the 2023 GMTK game Jam.

You can play the game [here](https://kronos328.itch.io/kill-the-skull-king)

# About

Skull King is the final boss of an RPG dungeon, where many players gather in parties to defeat him. Nerfed by the system, giving him no chance to dodge attacks, create OP combo's or even use some health potions, Skull King suffers to protect his lands from this treacherous adventurers, who seek only to get his crown for its +100% luck status.

In 'Kill the Skull King?' you are not the intrepidous hero who wants to save the world, but the almighty villain, with an insane amount of health and power that can easily defeat foes.


# How to play 

Left Click: Fire Basic Shot

Right Click: Launch Fireball

Spacebar: Use repel shield


# Credits

Ian Aguiar - Programmer

Henrique Tsuneda - Programmer

Lucas Santos - 3d Artist

Pedro Machado - Sound Designer
